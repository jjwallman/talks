\documentclass{article}
\usepackage[margin=21mm]{geometry}
\usepackage{xparse} 
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{color}
\usepackage{dsfont}
\pagestyle{empty}
%\usepackage{multicol}

\newcommand{\bs}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\md}[1]{\ensuremath{\mathds{#1}}}
\newcommand{\mc}[1]{\ensuremath{\mathcal{#1}}}
\newcommand{\mbb}[1]{\ensuremath{\mathbb{#1}}}
\newcommand{\ket}[1]{\ensuremath{|#1\rangle}}
\newcommand{\bra}[1]{\ensuremath{\langle #1 |}}
\newcommand{\dbra}[1]{\ensuremath{|#1\rangle\!\rangle}}
\newcommand{\dket}[1]{\ensuremath{\langle\!\langle #1 |}}

\DeclareMathOperator{\Tr}{Tr}

\DeclareDocumentCommand\op { m g } {%
	\IfNoValueTF {#2} {%
	\ensuremath{|#1\rangle\!\langle #1|}
	}{%
	\ensuremath{|#1\rangle\!\langle #2|}
	}%
}
\DeclareDocumentCommand\ip { m g } {%
	\IfNoValueTF {#2} {%
		\ensuremath{\langle #1|#1\rangle}
	}{%
		\ensuremath{\langle #1|#2\rangle}
	}%
}

\begin{document}

\begin{center}
{\huge Efficiently characterizing quantum gates and circuits}\\\vspace{5mm}
{\large Joel J. Wallman, Institute for Quantum Computing, University of Waterloo,
Canada}
\end{center}
\vspace{5mm}
\begin{minipage}[t]{.48\textwidth}
Quantum computers promise to revolutionize information processing, provided they
can be implemented with sufficiently small errors for the output to be reliable
with an efficient overhead\footnotemark. 
For quantum computers to be useful and reliable, estimating errors in quantum 
circuits has to use fewer classical computational resources than simulating an 
ideal quantum computer. \\

In this talk, I will discuss the primary ways of quantifying errors $\mc{E}$
in quantum circuits, namely, the average gate infidelity $r(\mc{E})$ and the 
diamond distance from the identity $\epsilon_\diamond(\mc{E})$
\footnotemark, and how they can be used to estimate the total error rate in a 
given circuit\footnotemark.
I will then discuss how these parameters can be estimated using randomized
benchmarking\footnotemark and purity benchmarking\footnotemark
by averaging outcome probabilities over random circuits of the form illustrated 
in Figure 1.\\

I will conclude by discussing how ideas from randomized benchmarking can be used
to reduce the error rate in quantum circuits\footnotemark, as demonstrated
in Figure 2.\\

a)\\
\includegraphics[width=.9\linewidth]{RB_circuit.pdf}\\
b)\\
\includegraphics[width=.9\linewidth]{unitarity_circuit.pdf}\\
Figure 1. Circuit for (a) randomized benchmarking; and (b) purity benchmarking, 
where the gates $g_1,\ldots,g_m$ are chosen uniformly at random from the 
Clifford group and $g_R = g_m^{-1} \ldots g_1^{-1}$. Let the probability
of a measurement outcome be $p(\vec{g},g_R)$ (for randomized benchmarking) or 
$p(\vec{g})$ (for purity benchmarking) for a fixed sequence 
$\vec{g}=(g_1,\ldots,g_m)$. Randomized and purity benchmarking experiments involve
estimating the probabilities $p(\vec{g},g_R)$ and $p(\vec{g})$ for multiple 
$\vec{g}$ for each of a variety of lengths $m$ and fitting to the models
\begin{align*}
\langle p(\vec{g},g_R)\rangle_{\vec{g}} = Ap^m + B\\
\langle p(\vec{g})\rangle_{\vec{g}} = A' u^m + B'
\end{align*}
where $A,A',B,B'$ are constants, the averages are over all sequences of $m$ gates
and $p$ and $u$ are parameters of the noise that allow error rates to be estimated.
\end{minipage}
\hfill
\begin{minipage}[t]{.48\textwidth}
\vspace{1mm}
\includegraphics[width=.9\linewidth]{Circuit_Diagram.png}

d)\\
 \includegraphics[width=.9\linewidth]{reduction.pdf}

Figure 2. (a)--(c) Circuits illustrating randomized compiling. (a) A standard
quantum circuit organized into alternating rounds of ``easy'' and ``hard'' gates
(e.g., single- and multi-qubit gates). (b) Random gates and the corresponding 
correction gates are introduced before and after each round of ``hard'' gates. 
(c) The random and correction gates are compiled into the adjacent easy gates,
resulting in a logically equivalent circuit with the same number of gates. 
Randomly sampling the random gates dramatically reduces the worst-case error,
as shown in (d).
\end{minipage}

\footnotetext[1]{A.~Kitaev, Russ.~Math.~Surv.~ \textbf{52}, 1191-1249 (1997).}

\footnotetext[2]{A.~Gilchrist, L.~Langford, and M.~A.~Nielsen, 
Phys. Rev. A \textbf{71}, 062310 (2005),
Y.~R.~Sanders, J.~J.~Wallman, and B.~C.~Sanders, 
New J. Phys. \textbf{18}, 012002 (2015).}

\footnotetext[3]{J.~J.~Wallman, arXiv:1511.00727,
A.~C.-Dugas, J.~J.~Wallman, and J.~Emerson, arXiv:1610.05296.}

\footnotetext[4]{E.~Knill et al, Phys. Rev. A \textbf{77}, 012307 (2008),
E.~Magesan, J.~M.~Gambetta, J.~Emerson, 
Phys.~Rev.~Lett.~\textbf{106}, 180504 (2011).}

\footnotetext[5]{J.~Wallman, C.~Granade, R.~Harper, S.~T.~Flammia, 
New J. Phys. \textbf{17} 113020 (2015).}

\footnotetext[6]{J.~J.~Wallman and J.~Emerson, Phys. Rev. A \textbf{94} 052325 (2016).}

\end{document}
