\documentclass[t]{beamer}

\include{macros}
\usepackage{parskip}
\setlength{\parskip}{5mm} 
\usetheme{Szeged}
\setbeamersize{text margin left=1em,text margin right=1em}
\setbeamertemplate{headline}{}

\title{Characterizing quantum computers}

\author{Joel Wallman}

\institute[Joel Wallman]{Institute for Quantum Computing}

\date[August 20, 2015]

\begin{document}

\begin{frame}[label=title]
	\titlepage
	
	\begin{center}
	47th Winter Colloquium on the Physics of Quantum Electronics
	\end{center}
\end{frame}

\begin{frame}
  \frametitle{Quantum computers}	
\begin{columns}
\begin{column}{.45\linewidth}
	\begin{center}
Ideal quantum computer\\\vspace{2mm}
\Qcircuit @C=.4em @R=.4em @! {
& \lmeter & \qw & \gate{H} & \ctrl{1} & \qw & \rstick{|\psi\ra}\qw \\
& \cwx & \lmeter & \qw & \targ & \targ & \rstick{|0\ra}\qw \\
& \gate{Z} \cwx & \gate{X} \cwx & \qw & \qw & \ctrl{-1}& \rstick{|+\ra}\qw 
}
\vspace{5mm}
\uncover<2->{Can solve hard problems!}
\end{center}
\end{column}
\vrule
\begin{column}{.45\linewidth}
\begin{center}
\uncover<3->{Real quantum computer\footnotemark\\\vspace{2mm}}
\uncover<4->{\centering\includegraphics[width=.65\linewidth]{falle.png}}\\
\uncover<5->{Can factor special or small numbers}
\end{center}
\end{column}
\end{columns}
\uncover<6->{Unwanted interactions and imperfect control change the 
probabilities of measurement outcomes}
\footnotetext[1]{\uncover<4->{Photo of an ion trap at the University of 
Innsbruck, Austria}}
\end{frame}

\begin{frame}
\frametitle{What's in an error?}
A tale of two t-errors: amplitude (T1) and phase (T2) damping.

\begin{minipage}{.3\linewidth}
\uncover<2->{\centering\includegraphics[width=.95\linewidth]{falle.png}}
\end{minipage}
\begin{minipage}{.6\linewidth}
\flushleft
\uncover<3->{Stochastic errors: $\mc{E}(\rho) = \sum_i p_i \sigma_i \rho 
\sigma_i$\\\vspace{5mm}}
\uncover<4->{Unitary errors: $\mc{E}(\rho) = U\rho U^{\dagger} $\\\vspace{5mm}}
\uncover<5->{Completely positive, trace-preserving maps $\mc{E}$\\\vspace{5mm}}
\uncover<6->{Non-Markovian noise\\\vspace{5mm}}
\end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Characterizing errors}	

Can completely characterize noise and calculate the probability of getting the
right answer under actual noise.

\uncover<2->{But: more expensive than simulating an ideal quantum computer!}

\uncover<3->{Mission statement for efficient QCVV:}
\begin{itemize}
\item<4-> partially characterize large-scale quantum systems;
\item<5-> construct \textit{useful} bounds on the probability of error; and
\item<6-> estimate the amount of overhead required to perform quantum computations
at a target precision.
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Errors in quantum circuits\footnote{\textbf{JJW}, arXiv:1511.00727 (2015)}}

Ideal circuit: $p(m)=$ \only<1>{\Qcircuit @C=.4em @R=.4em @! {
\lmeter & \gate{G_m} & \qw & \ldots & & \gate{G_1} & \rstick{\psi}\qw}}
\only<2->{\Qcircuit @C=.4em @R=.4em @! {
\lmeter & \gate{\only<2>{G_m\ldots G_1}\only<3->{G_{m:1}}} & \rstick{\psi}\qw}}

\uncover<4->{Noisy circuit: $\tilde{p}(m)=$ \Qcircuit @C=.4em @R=.4em @! {
\lmeter & \gate{\tilde{G}_{m:1}} & \rstick{\psi}\qw}}

\uncover<5->{For circuit to be reliable, need\\ \vspace{5mm}
$\tilde{p}(m)-p(m)\approx \sum_j$ 
\Qcircuit @C=.4em @R=.4em @! {\lmeter & \gate{G_{m:j+1}} & \gate{\tilde{G}_j-G_j} & \gate{G_{j-1:1}} & \rstick{\psi}\qw}\\\vspace{5mm}
to be small.}
\end{frame}

\begin{frame}
	\frametitle{Errors in trivial quantum circuits}

Suppose the circuit is the identity and we prepare and measure $\psi$.

\uncover<2->{Then the error is\\\vspace{5mm}
$\tilde{p}(m)-p(m)\approx \sum_j$ \hspace{7mm}
\only<2>{\Qcircuit @C=.4em @R=.4em @! {\lstick{\psi} & \gate{G_{m:j+1}} & 
\gate{\tilde{G}_j-G_j} & \gate{G_{j-1:1}} & \rstick{\psi}\qw}}}
\only<3>{\Qcircuit @C=.4em @R=.4em @! {\lstick{\psi} & \gate{U^{\dagger} G_j^{\dagger} } &
\gate{\tilde{G}_j-G_j} & \gate{U} & \rstick{\psi}\qw}}
\only<4->{\Qcircuit @C=.4em @R=.4em @! {\lstick{U(\psi)} & 
\gate{G_j^{\dagger} \tilde{G}_j - I} & \rstick{U(\psi)}\qw}}

\uncover<5->{That is, $\tilde{p}(m)-p(m) = \sum_j \tr \phi [G_j^{\dagger} \tilde{G}_j - I](\phi) = \sum_j r(G_j^{\dagger} \tilde{G}_j,\phi)$.}

\uncover<6->{The average gate infidelity is $r(G_j^{\dagger} \tilde{G}_j) = \int \mr{d}\phi r(G_j^{\dagger} \tilde{G}_j,\phi)$,
and $r(G_j^{\dagger} \tilde{G}_j,\phi)\leq (d+1)r(G_j^{\dagger} \tilde{G}_j)$.}
\end{frame}

\begin{frame}
	\frametitle{\textit{Characterizing} errors in trivial quantum circuits}

The average gate infidelity $r(G_j^{\dagger} \tilde{G}_j)$ quantifies errors in trivial
circuits quite well, how do we estimate it?

\uncover<2->{Basic principle: estimate small errors by amplifying them!}

\uncover<3->{Previous analysis only holds for small errors, but can amplify
errors using randomized benchmarking}
\end{frame}

\begin{frame}
\frametitle{Randomized benchmarking protocol~\footnote{Magesan, Gambetta and 
Emerson, \textit{Phys. Rev. A} \textbf{85} 042311 (2012).}}
Can estimate $\ds{E}_G r(G^{\dagger} \tilde{G})$ averaged over a 2-design $\mbb{G}$.
\begin{enumerate}\setlength\itemsep{0mm}
	\item<2-> Randomly choose $m$ gates, $\vec{G}= (G_1,\ldots,G_m)$ and set 
	$G_{\rm inv} = (G_{m:1})^{\dagger} $.
	\item<3-> Estimate the expectation value $Q_{\vec{G}}$ after 
	preparing $\rho$ and applying the sequence of gates $G_1$, $\ldots$, $G_m$, 
	$G_{\rm inv}$.
	\item<4-> Repeat for different sequences and lengths to estimate 
	$\mathds{E}_{\vec{G}} Q_{\vec{G}}$.
	\item<5-> Fit to $\mathds{E}_{\vec{G}}Q_{\vec{G}} = A 
	p(G^{\dagger} \tilde{G})^m + 	B$, where $1-p = dr/(d-1)$.
\end{enumerate}
\uncover<6->{The random gates reduces any noise to depolarizing noise.}
\end{frame}

\begin{frame}
	\frametitle{Errors in \textit{nontrivial} quantum circuits\footnote{\textbf{JJW}, arXiv:1511.00727 (2015)}}

For circuit to be reliable, need
$\delta p = \tilde{p}(m)-p(m)\approx \sum_j \tr Q (G_j^{\dagger} \tilde{G}_j-I)(\psi)$
to be small.

\begin{minipage}[t]{.55\linewidth}
\begin{center}
Error in 3 qubit circuits with 50 gates
\end{center}
\includegraphics<2->[height=45mm]{fidelityscaling.pdf}
\end{minipage}
\hfill
\begin{minipage}[t]{.4\linewidth}
\uncover<3->{$Q$ and $\psi$ are no longer in the same basis!\\}

\uncover<4->{$\tr Q (G_j^{\dagger} \tilde{G}_j-I)(\psi)$ can differ
significantly from the infidelity.}
\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Efficiently characterizing errors in non-trivial circuits
\footnote{\textbf{JJW}, C. Granade, R. Harper, S. T. Flammia, NJP 17 113020 (2015).}}
The infidelity and mean $\tr Q (G_j^{\dagger} \tilde{G}_j-I)(\psi)$ differ significantly
due to seemingly negligible coherent errors (e.g., $1\%$ of the infidelity).

\uncover<2->{Infidelity of a noise channel $\mc{E}$ alone not enough.}

\uncover<3->{The unitarity of $\mc{E}$ is $u(\mc{E}) = \int {\rm d}\psi\, \tr \mc{E}(\psi - 
\tfrac{1}{d}\mathds{I}_d)^2$.}

\uncover<4->{The infidelity and the unitarity provide upper and lower bounds on 
the worst- and average-case errors that differ by dimensional factors and the
reduction in the infidelity that can be obtained by improving control.}

\uncover<5->{The unitarity can be efficiently estimated using a variant of randomized benchmarking}
\end{frame}

\begin{frame}
\frametitle{Removing coherent errors via Randomized 
Compiling\footnote{\textbf{JJW} and 
J. Emerson, arXiv:1512.01098}}
	
	\vspace{\stretch{1}}
	
	\begin{minipage}[c][.5\textheight][c]{0.6\textwidth}
		
		\begin{itemize}\setlength{\itemsep}{1mm}
			\item<1-> Express circuit as alternating rounds of ``easy'' 
			and ``hard'' gates
			
			\item<2-> Insert single-qubit twirling gates and correction gates 
			in each round
			
			\item<3-> Compile them in
		\end{itemize}
	\vspace{5mm}
\uncover<4->{E.g., physical circuit, logical circuit with transversal 
Cliffords,}
\begin{itemize}
	\item<5-> easy gates = $\langle X,Z,R_Z(\pi/4)\rangle$
	\item<6-> twirling gates = Pauli gates
	\item<7-> hard gates = $\{\Delta(Z), R_Z(\pi/8), H\}$
\end{itemize}
		\vfill

	\end{minipage}%
	\begin{minipage}[c]{0.4\textwidth}
		\vspace{\stretch{1}}
		\begin{center} 
			\includegraphics[width=0.95\linewidth]{Circuit_Diagram.png}
		\end{center}
		\vspace{\stretch{1}}
	\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Benefits of noise tailoring}

\begin{minipage}[c][.6\textheight][c]{0.6\textwidth}
\begin{itemize}\setlength{\itemsep}{1mm}
	\item<1-> No coherent errors
	\item<2-> Robust to independent and arbitrary errors on the hard gates.
	\item<3-> Gate depth of circuit does not change!
	\item<4-> Compiled circuit can be precomputed or computed on the fly with 
	fast classical control.
\end{itemize}
\vfill
\end{minipage}%
\begin{minipage}[c]{0.4\textwidth}
\vspace{\stretch{1}}
\begin{center} 
	\includegraphics[width=0.95\linewidth]{Circuit_Diagram.png}
\end{center}
\vspace{\stretch{1}}
\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Numerical improvements for randomized compiling}
\begin{align*}
\label{eq:tvd}
\tau_{\rm noise} = \sum_m |{\rm Pr}(m|{\rm noise}) - {\rm Pr}(m|{\rm id})|,
\end{align*}
	
\begin{minipage}{.45\linewidth}
	\begin{center} 
		\vspace{-5mm}
		\includegraphics[width=0.95\linewidth]{varying_noise.pdf}
	\end{center}
\end{minipage}
\hfil
\begin{minipage}{.45\linewidth}
	\begin{center} 
		\vspace{-5mm}
		\includegraphics[width=0.95\linewidth]{varying_ngates.pdf}
	\end{center}
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
\item Errors in trivial circuits can be easily characterized using randomized
benchmarking
\item<2-> Errors in non-trivial circuits scale differently in the presence of 
seemingly negligible coherent noise
\item<3-> Can efficiently quantify the coherence of noise using randomized
benchmarking and a variant thereof.
\item<4-> Obtain upper- and lower- bounds on average and worst case errors
that differ by dimensional factors.
\item<5-> We can turn unitary errors into stochastic errors using randomized 
compiling at virtually no cost!
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Acknowledgements}
	\begin{center}
	\includegraphics[height=20mm]{IQC_LOGO.png}\\\vspace{5mm}
	\includegraphics[height=20mm]{ARO.jpg}\hspace{5mm}\includegraphics[height=20mm]{ARC.jpg}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{References}
	\begin{itemize}
		\item \textbf{JJW}, C. Granade, R. Harper, S. T. Flammia, 
		``\textit{Estimating the Coherence of Noise}'', NJP 17 113020 
		(2015)
		\item Y. Sanders, \textbf{JJW}, B. Sanders, ``\textit{Bounding quantum 
		gate error rate based on reported average fidelity}'', NJP 18 
		012002 (2016)
		\item \textbf{JJW}, J. Emerson, ``\textit{Noise tailoring for scalable 
		quantum computation via randomized compiling}'', arXiv:1512.01098
		\item \textbf{JJW}, ``\textit{Bounding experimental quantum error rates 
		relative to fault-tolerant thresholds}'', arXiv:1511.00727
		\item G. Feng \textit{et al.}, ``\textit{Estimating the coherence of 
		noise in quantum control of a solid-state qubit}'', arXiv:1603.03761
	\end{itemize}
\end{frame}

\end{document}