\documentclass[t]{beamer}

\include{macros}
\include{beamermacros}

\title{Characterizing large-scale quantum computers}

\author{Joel Wallman}

\institute[Joel Wallman]{Institute for Quantum Computing}

\date[May 2, 2017]

\begin{document}

\begin{frame}[label=title]
	\titlepage
\end{frame}

\begin{frame}
  \frametitle{Quantum computers}	
\begin{columns}
\begin{column}{.45\linewidth}
	\begin{center}
Ideal quantum computer\\\vspace{2mm}
\Qcircuit @C=.4em @R=.4em @! {
& \lmeter & \qw & \gate{H} & \ctrl{1} & \qw & \rstick{|\psi\ra}\qw \\
& \cwx & \lmeter & \qw & \targ & \targ & \rstick{|0\ra}\qw \\
& \gate{Z} \cwx & \gate{X} \cwx & \qw & \qw & \ctrl{-1}& \rstick{|+\ra}\qw 
}
\vspace{5mm}
\uncover<2->{Can solve hard problems!}
\end{center}
\end{column}
\vrule
\begin{column}{.45\linewidth}
\begin{center}
\uncover<3->{Real quantum computer\footnotemark\\\vspace{2mm}}
\uncover<4->{\centering\includegraphics[width=.65\linewidth]{falle.png}}\\
\uncover<5->{Can factor special or small numbers}
\end{center}
\end{column}
\end{columns}
\uncover<6->{Unwanted interactions and imperfect control change the 
probabilities of measurement outcomes}\uncover<7->{ $\Longrightarrow$ computational errors}
\footnotetext[1]{\uncover<4->{Photo of an ion trap at the University of 
Innsbruck, Austria}}
\end{frame}

\begin{frame}
\frametitle{What's in an error?}
Traditionally, focus has been on amplitude (T1) and phase (T2) damping.

\begin{minipage}{.3\linewidth}
\uncover<2->{\centering\includegraphics[width=.95\linewidth]{falle.png}}
\end{minipage}
\begin{minipage}{.6\linewidth}
\flushleft
\uncover<3->{Stochastic errors: $\cE(\rho) = \sum_i p_i \sigma_i \rho 
\sigma_i$\\\vspace{5mm}}
\uncover<4->{Unitary errors: $\cE(\rho) = U\rho U^{\dagger} $\\\vspace{5mm}}
\uncover<5->{Completely positive, trace-preserving maps $\cE$\\\vspace{5mm}}
\uncover<6->{Non-Markovian noise\\\vspace{5mm}}
\end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Characterizing errors}	

Can completely characterize noise and calculate the probability of getting the
right answer under actual noise.

\uncover<2->{But: more expensive than simulating an ideal quantum computer!}

\uncover<3->{Mission statement for efficient QCVV:}
\begin{itemize}
\item<4-> partially characterize large-scale quantum systems;
\item<5-> construct \textit{useful} bounds on the probability of error; and
\item<6-> estimate the amount of overhead required to perform quantum computations at a target precision.
\end{itemize}

\uncover<7->{The only known approaches are based on randomized benchmarking}
\end{frame}

\begin{frame}
	\frametitle{Errors in quantum circuits\footnotemark[2]}

For circuit to be reliable, need $\delta p = p_{noisy}(m)-p_{ideal}(m)$ to be small.

\uncover<2->{Different noise models have substantially different effects on $\delta p$ for any given error metric.}\\
\begin{minipage}[t]{.45\textwidth}
\uncover<3->{Variation is even more pronounced with quantum error correction\footnotemark[3]}\\

\uncover<4->{Stochastic noise generally has a smaller impact and is easier to predict\footnotemark[2]\footnotemark[4]}
\end{minipage}
\hspace{5mm}
\begin{minipage}[t]{.45\textwidth}
\uncover<3->{\begin{center}
Error in 3 qubit circuits,\\
50 noisy gates with infidelity $r$
\includegraphics[height=35mm]{fidelityscaling.pdf}
\end{center}}
\end{minipage}
\footnotetext[2]{\textbf{JJW}, arXiv:1511.00727 (2015)}
\footnotetext[3]{David Poulin, unpublished}
\footnotetext[4]{Magesan, Gambetta and 
Emerson, Phys. Rev. A \textbf{106} 180504 (2012).}
\end{frame}

\begin{frame}
\frametitle{Removing coherent errors via Randomized 
Compiling\footnote{\textbf{JJW} and 
J. Emerson, Phys. Rev. A 94, 052325 (2016)}}
	
	\vspace{\stretch{1}}
	
	\begin{minipage}[c][.5\textheight][c]{0.6\textwidth}
		
		\begin{itemize}\setlength{\itemsep}{1mm}
			\item<1-> Express circuit as alternating rounds of ``easy'' 
			and ``hard'' gates
			
			\item<2-> Insert single-qubit twirling gates and correction gates 
			in each round
			
			\item<3-> Compile them in
		\end{itemize}
	\vspace{5mm}
\uncover<4->{E.g., physical circuit, logical circuit with transversal 
Cliffords,}
\begin{itemize}
	\item<5-> easy gates = $\langle X,Z,R_Z(\pi/4)\rangle$
	\item<6-> twirling gates = Pauli gates
	\item<7-> hard gates = $\{\Delta(Z), R_Z(\pi/8), H\}$
\end{itemize}
		\vfill

	\end{minipage}%
	\begin{minipage}[c]{0.4\textwidth}
		\vspace{\stretch{1}}
		\begin{center} 
			\includegraphics[width=0.95\linewidth]{Circuit_Diagram.png}
		\end{center}
		\vspace{\stretch{1}}
	\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Benefits of noise tailoring}

\begin{minipage}[t][.6\textheight][c]{0.6\textwidth}
\begin{itemize}\setlength{\itemsep}{1mm}
	\item<1-> No coherent errors
	\item<2-> Error rate dictated by the average gate infidelity for Markovian noise.
	\item<3-> Robust to independent and arbitrary errors on the hard gates.
	\item<4-> Gate depth of circuit does not change!
	\item<5-> Compiled circuit can be precomputed or computed on the fly with 
	fast classical control.
	\item<6-> Preliminary: appears to dynamically decouple non-Markovian noise.
\end{itemize}
\vfill
\end{minipage}%
\begin{minipage}[t]{0.4\textwidth}
\vspace{\stretch{1}}
\begin{center} 
	\includegraphics[width=0.95\linewidth]{Circuit_Diagram.png}
\end{center}
\vspace{\stretch{1}}
\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Numerical improvements for randomized compiling}
\begin{align*}
\tau_{\rm noise} = \sum_m |p_{noisy}(m) - p_{ideal}(m)|,
\end{align*}
	
\begin{minipage}{.45\linewidth}
	\begin{center} 
		\vspace{-5mm}
		\includegraphics[width=0.95\linewidth]{varying_noise.pdf}
	\end{center}
\end{minipage}
\hfil
\begin{minipage}{.45\linewidth}
	\begin{center} 
		\vspace{-5mm}
		\includegraphics[width=0.95\linewidth]{varying_ngates.pdf}
	\end{center}
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Randomized benchmarking\footnote{Magesan, Gambetta, and 
Emerson, Phys. Rev. Lett. \textbf{106} 180504 (2011).}}
Estimate average gate infidelity $r$ of the noise between gates in a unitary 2-design $\bbG$.\footnote[6]{\textbf{JJW}, arXiv:1703.09835.}
\begin{enumerate}\setlength\itemsep{0mm}
	\item<2-> Randomly choose $m$ gates, $\vec{G}= (G_1,\ldots,G_m)$ and set 
	$G_{\rm inv} = (G_{m:1})\ct $.
	\item<3-> Estimate the expectation value $Q_{\vec{G}}$ after 
	preparing $\rho$ and applying the sequence of gates $G_1$, $\ldots$, $G_m$, 
	$G_{\mathrm{inv}}$.
	\item<4-> Repeat for different sequences and lengths to estimate 
	$\bbE_{\vec{G}} Q_{\vec{G}}$.
	\item<5-> Fit to $\bbE_{\vec{G}}Q_{\vec{G}} = A 
	p(G^{\dagger} \tilde{G})^m + 	B$, where $1-p = dr/(d-1)$.
\end{enumerate}
\uncover<6->{The random gates reduces any noise to depolarizing noise.}
\end{frame}

\begin{frame}
\frametitle{Randomized benchmarking and randomized compiling}

Randomized benchmarking gives you the average error rate for implementing a circuit of gates in $\bbG$ via randomized compiling using $\bbG$ as the set of twirling gates and no hard gates.

\uncover<2->{There is no known 2-design $\bbG$ that can efficiently be used as the twirling gates in randomized compiling to achieve universality}

\uncover<3->{Examples:}
\begin{itemize}
\item<4-> The full unitary group: correction gates cannot be efficiently computed.
\item<5-> The Clifford group: adding any unitary generates the full unitary group, so calculating the correction gates for any non-trivial hard gates is probably inefficient.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Dihedral benchmarking\footnote{Carignan-Dugas, \textbf{JJW}, and
Emerson, Phys. Rev. A \textbf{92}, 060302 (2015).}}
Estimate average gate infidelity $r$ of the noise between gates in the dihedral group $\bbD_4 = \langle X,Z,R_Z(\pi/4)\rangle$.
\begin{enumerate}\setlength\itemsep{-2mm}
	\item<2-> Randomly choose $m$ gates, $\vec{G}= (G_1,\ldots,G_m)\in\bbD_4^m$ and a bit $b$ and set $G_{\rm inv} = Z^b(G_{m:1})\ct $.
	\item<3-> Estimate the expectation value $Q_{\vec{G}}$ after 
	preparing $\rho$ and applying the sequence of gates $G_1$, $\ldots$, $G_m$, 
	$G_{\mathrm{inv}}$.
	\item<4-> Repeat for different sequences and lengths to estimate 
	$\bbE_{\vec{G}} Q_{\vec{G}}$.
	\item<5-> Fit to $\bbE_{\vec{G}}Q_{\vec{G}} = A 
	p_Z(G^{\dagger} \tilde{G})^m + 	B$.
\end{enumerate}
\uncover<6->{The random gates reduces any noise to Pauli noise.}

\uncover<7->{Repeat for $G_{\rm inv} = X^b(G_{m:1})\ct $ to obtain $p_X$, $r = (3-p_Z-2p_X)/6$.}
\end{frame}

\begin{frame}
\frametitle{Experimental results from Rainer Blatt's group}
\begin{center}
\includegraphics[height = 75mm]{2017_DB_1qubit_summary.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Benchmarking the MS gate}

Standard randomized benchmarking randomizes over multi-qubit gates, making it difficult to isolate an error on a fixed multi-qubit gate.

\uncover<2->{Isolate error rates on multi-qubit gates such as the CZ, CNOT, MS $=\exp(i \pi ZZ/4)$ with a precision determined by single-qubit gates by:}
\begin{itemize}
\item<3-> performing independent dihedral benchmarking on two qubits;
\item<4-> repeating with the multi-qubit gate interleaved between all dihedral gates and the correction gates updated accordingly.
\end{itemize}

\uncover<5->{Currently developing a modification to provide a rigorous set of tune-up sequences for multi-qubit gates}

\uncover<6->{Provides information about spatial correlations in the noise.}
\end{frame}

\begin{frame}
\frametitle{Experimental results from Rainer Blatt's group}
\begin{center}
\includegraphics[height = 40mm]{db_summary.pdf}
\includegraphics[height = 40mm]{20170323_DB_2qubit_80_bar2.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
\item Randomized benchmarking protocols are the only known scalable protocols for characterizing quantum systems.
\item<2-> Randomized benchmarking protocols can be applied to both physical and logical qubits.
\item<3-> The estimates from randomized benchmarking protocols closely characterize the performance of circuits implemented by randomized compiling.
\item<4-> Can generalize randomized benchmarking to apply to other useful gate sequences and directly characterize multi-qubit gates.
\item<5-> Modified randomized benchmarking sequences can be used as tune-up sequences for multi-qubit gates.
\end{itemize}
\end{frame}

\end{document}