\documentclass[t]{beamer}

\include{beamermacros}
\setlength{\tgskip}{20mm}

\title{Error correction under general noise}
\subtitle{arXiv:1612.02830}
\author[Joel Wallman]{Joel Wallman\\
{\scriptsize Joint work with Christopher Chamberland, Stefanie Beale, and Raymond Laflamme}}

\institute[Joel Wallman]{Institute for Quantum Computing}

\date[August 20, 2015]

\begin{document}

\maketitle

\begin{frame}
\frametitle{Stabilizer codes}

An $n$-qubit rank-$k$ stabilizer group $\mc{S}_{k,n}$ is the set of all
products of $k$ independent, commuting Pauli operators $\{g_1,\ldots, g_k\}$.

\uncover<2->{An $[\![n,k,d]\!]$ stabilizer code is the $+1$-eigenspace of some $\mc{S}_{k,n}$.}

\uncover<3->{A logical operator commutes with everything in $\mc{S}_{k,n}$ but is not in $\mc{S}_{k,n}$.}

\uncover<4->{
\begin{center}
\begin{tabular}{ |c|c|c|c|}\hline
 5-qubit code & Steane code & Z-Shor code & Surface-17 code \\ \hline
     XZZXI      &   IIIZZZZ     & ZZIIIIIII & ZIIZIIIII \\
     IXZZX      &   IZZIIZZ     & ZIZIIIIII & IZZIZZIII \\
     XIXZZ      &   ZIZIZIZ     & IIIZZIIII & IIIZZIZZI \\
     ZXIXZ      &    IIIXXXX   & IIIZIZIII & IIIIIZIIZ \\
				&   IXXIIXX     & IIIIIIZZI & XXIXXIIII \\
                &   XIXIXIX    & IIIIIIZIZ & IXXIIIIII \\
                &                 &\ XXXXXXIII \ &  IIIIXXIXX \\
                &                 &\ IIIXXXXXX \ &  IIIIIIXXI  \\\hline
\end{tabular}
\end{center}}
\end{frame}

\begin{frame}
\frametitle{Decoding}
Suppose a physical Pauli error $E$ acts on $|\overline{\psi}\ra$. 
\uncover<2->{Measuring the $\{g_i\}$ produces the syndrome 
$l=l_{1}l_{2}\hdots l_{n-k}$ where $Eg_i = (-1)^{l_i} g_i E$.}

\uncover<3->{Applying a Pauli recovery operator that gives the syndrome $l$
returns $E|\overline{\psi}\ra$ to the code space,}
\uncover<4->{but possibly with a logical error.}

\uncover<5->{A \textit{decoding algorithm} is a method of obtaining a Pauli recovery map $R_l$ for 
each syndrome $l$.}

\uncover<6->{A decoding algorithm is optimal for a noise model if it
results in the lowest probability of a logical error.}

\uncover<7->{E.g., the errors $X_1$ and $X_2 X_3$ give the same syndrome
for two stabilizers $S_{1} = Z_{1}Z_{2}$ and $S_{2} = Z_{2}Z_{3}$. For
uncorrelated noise, the better choice is $R_{1,0} = X_1$.}
\end{frame}

\begin{frame}
\frametitle{Logical noise}
Encoding map is $\mc{E}(\rho_{in}) = B \rho_{in} B\ct$ where 
$B = |\overline{0}\ra\!\la 0| + |\overline{1}\ra\!\la 1|$.

\uncover<2->{Effective channel is
$\mc{G}(\mc{N}) =\sum_l \mc{E}\ct \mc{R}_{l} \mc{NE}$.}

\uncover<3->{Concatenation: $\mc{G}^{(t)}(\mc{N}) = \mc{G}[\mc{G}^{(t-1)}(\mc{N})]$
with $\mc{G}^{(0)} = \mc{N}_p$.}

\uncover<4->{Not all the $\mc{G}_l(\mc{N}) = \mc{E}\ct \mc{R}_{l} \mc{NE}$ are distinct!}

\uncover<5->{
\begin{center}
\begin{tabular}{ |c|c|c|c|c|c|c|}\hline
code & $[\![5,1,3]\!]$ & Steane & Shor & surface-9 & RM-15 & color-17 \\ \hline
$\#$ $\mc{G}^{(l)}$ & 16      &   64     & 256 & 256 & 16384 & 65536 \\ \hline
$\#$ distinct & 4      &   5     & 10 & 45 & 23 & 106 \\ \hline
\end{tabular}
\end{center}}
\end{frame}

\begin{frame}
\frametitle{Optimized hard decoding}
\begin{minipage}[t]{.49\linewidth}
Let:
\begin{itemize}\setlength{\itemsep}{5mm}
\item $\epsilon$ be an ``error rate'';
\item<2-> $\{\mc{R}_{l,t}\}$ be recovery operators
minimizing $\epsilon\Bigl(\mc{G}^{(t-1)}(\mc{N}_p)\Bigr)$;
\item<3->$\mc{G}_{opt}^{(t)}(\mc{N}_p) = \sum_l \mc{E}\ct \mc{R}_{l,t} \mc{G}^{(t-1)}(\mc{N})\mc{E}$.
\end{itemize}
\vspace{5mm}
\uncover<5->{When $\epsilon$ is the average gate infidelity, can maximize $\tr \mc{G}^{(t-1)}(\mc{N}_p,\mc{R}_l)$
independently!}\\

\uncover<6->{Can use transversal gates for recovery maps.}
\end{minipage}
\hspace{1mm}
\begin{minipage}[t]{.48\linewidth}
\uncover<4->{\includegraphics[height=65mm]{pdfofflowchart.pdf}}
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Threshold hypersurface for generalized damping channels}
\begin{center}
\includegraphics[height=50mm]{AmpPhaseDampThresholdAllPlots.png}
\begin{align*}
\mathcal{N}_{APD}(\rho) = \mathcal{N}_{PD}(\mathcal{N}_{AD}(\rho))=\mathcal{N}_{AD}(\mathcal{N}_{PD}(\rho)) \notag\\
\uncover<2->{A^{(0)}_{AD}= |0\ra\!\la 0| + \sqrt{1-p}|1\ra\!\la 1|, \hspace{0.1cm} 
A^{(1)}_{AD}= \sqrt{p}|0\ra\!\la 1|} \\
\uncover<3->{A^{(0)}_{PD}=|0\ra\!\la 0| + \sqrt{1-\lambda}|1\ra\!\la 1|, \hspace{0.1cm} 
A^{(1)}_{PD}= \sqrt{\lambda}|1\ra\!\la 1|}
\end{align*}
\end{center}
\end{frame}

\begin{frame}
\frametitle{$[\![5,1,3]\!]$ threshold hypersurface for coherent errors}
\includegraphics[height=50mm]{513coherentsym.png}
\hspace{1mm}
\includegraphics[height=50mm]{513coherentopt.png}

$\mc{N}(\rho) = \exp(i\theta \hat{n}\cdot\sigma) \rho \exp(-i\theta \hat{n}\cdot\sigma)$ where
$\hat{n}=(\sin\phi\cos\gamma,\sin\phi\sin\gamma,\cos\phi)$.

\uncover<2->{Transversal gates give a large boost!}
\end{frame}

\begin{frame}
\frametitle{$[\![5,1,3]\!]$ logical fidelity for coherent errors}
\begin{center}
\includegraphics[height=50mm]{5qubitInfidelitiesCoherent.png}

Any rotation around $\hat{n} \propto (1,1,1)$ can be corrected.
\end{center}
\end{frame}

\begin{frame}
\frametitle{Pauli twirling}
\begin{center}
\includegraphics[height=35mm]{PauliTwirlPlotSteane.png}

Threshold values for rotations about $(\tfrac{1}{\sqrt{2}}\sin\phi,\tfrac{1}{\sqrt{2}}\sin\phi,\cos\phi)$
by $\theta$ under symmetric, optimal Pauli and optimal transversal recovery maps.

\uncover<2->{Turning coherent errors into stochastic errors can hurt performance!}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Robustness to perturbations}
\includegraphics[height=35mm]{AmpPhaseDampDecoderRobust.png}
\hspace{1mm}
\uncover<2->{\includegraphics[height=35mm]{5qubitPerturbPlotLogScale.png}}

Noise is never known perfectly. How does a decoder optimized for a specific 
model perform for nearby noise?

\uncover<3->{Depends on how you quantify `nearby'.}
\end{frame}

\begin{frame}
\frametitle{Conclusion}

\end{frame}

\end{document}